# Stage 0
FROM node:12-slim AS build

WORKDIR /app
COPY ./package.json ./package-lock.json ./
RUN npm ci

COPY . .
RUN npm run build

# Stage 1
FROM nginx:1.17
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build/ /usr/share/nginx/html/
