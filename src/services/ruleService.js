import { post, get, put } from '../utils/requests';
import * as LocalStorageService from "./LocalStorageService"

var accessToken = LocalStorageService.getValue("accessToken")

export const getRules = ({accessToken, onSuccess, onError}) => {
  get({
    headers: {'x-auth-token': accessToken },
    apiRoute: '/api/rules'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al obtener las reglas: ', error));
}

export const getRule = ({ruleId, onSuccess, onError}) => {
  return (
    get({ 
      headers: {'x-auth-token': accessToken},
      apiRoute: '/api/rules/' + ruleId
    })
    .then(res => onSuccess(res))
    .catch(error => onError('Error al obtener la regla: ', error))
  )
}

export const createRule = ({accessToken, rule, onSuccess, onError}) => {
  return(
    post({ 
      body: rule, 
      headers: {'x-auth-token': accessToken},
      apiRoute: '/api/rules'
    })
    .then(res => onSuccess(res))
    .catch(error => onError('Error al crear la regla: ', error))
  )
}

export const enable = ({accessToken, ruleId, onSuccess, onError}) => {
  return(
    put({ 
      headers: {'x-auth-token': accessToken},
      apiRoute: `/api/rules/${ruleId}/enable`
    })
    .then(res => onSuccess(res))
    .catch(error => onError('Error al activar la regla: ', error))
  )
}

export const disable = ({accessToken, ruleId, onSuccess, onError}) => {
  return(
    put({ 
      headers: {'x-auth-token': accessToken},
      apiRoute: `/api/rules/${ruleId}/disable`
    })
    .then(res => onSuccess(res))
    .catch(error => onError('Error al desactivar la regla: ', error))
  )
}




