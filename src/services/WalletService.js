import { get, post, deleteMethod } from '../utils/requests';
import CryptoJS from 'crypto-js'

const baseURL = "https://testnet.binance.vision";

export const getBalance = ({wallet, onSuccess, onError}) => {

  const {
    apiKey,
    apiSecret: secretKey
  } = wallet;

  var date = new Date();
  var timestamp = date.getTime();

  const signature = CryptoJS.HmacSHA256(
  `timestamp=${timestamp}`,
    secretKey
  ).toString(CryptoJS.enc.Hex)

  return (
    get({ 
      baseURL: baseURL,
      apiRoute: `/api/v3/account?timestamp=${timestamp}&signature=${signature}`,
      headers: {
        "X-MBX-APIKEY": apiKey
      }
    })
    .then(res => onSuccess(res))
    .catch(error => onError("Error al traer los balances: ", error))
  )
}

export const createWallet = ({accessToken, body, onSuccess, onError}) => {
  return (
    post({ 
      apiRoute: `/api/wallets/`,
      body,
      headers: {'x-auth-token': accessToken }
    })
    .then(res => onSuccess(res))
    .catch(e => onError(e))
  )
}

export const deleteWallet = ({accessToken, onSuccess, onError}) => {
  return (
    deleteMethod({ 
      apiRoute: `/api/wallets`,
      headers: {'x-auth-token': accessToken }
    })
    .then(res => onSuccess(res))
    .catch(e => onError(e))
  )
}