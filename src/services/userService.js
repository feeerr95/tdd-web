import { put, get, post } from '../utils/requests'

export const signUp = ({body, onSuccess, onError}) => {
  post({
    body,
    apiRoute: '/api/users/signUp'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al registrarse: ', error));
}

export const signIn = ({body, onSuccess, onError}) => {
  post({
    body,
    apiRoute: '/api/users/signIn'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al logearse: ', error));
}

export const signOut = ({accessToken, onSuccess, onError}) => {
  post({
    headers: {'x-auth-token': accessToken },
    apiRoute: '/api/users/signOut'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al deslogearse: ', error));
}

export const getUser = ({accessToken, onSuccess, onError}) => {
  get({
    headers: {'x-auth-token': accessToken },
    apiRoute: '/api/users/me'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al obtener el usuario: ', error));
}

export const createVariable = ({accessToken, variable, onSuccess, onError}) => {
  put({
    body: variable,
    headers: {'x-auth-token': accessToken },
    apiRoute: '/api/users/variables'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al obtener el usuario: ', error));
}

export const updateVariable = ({accessToken, variable, onSuccess, onError}) => {
  put({
    body: variable,
    headers: {'x-auth-token': accessToken },
    apiRoute: '/api/users/variables'
  })
  .then(res => onSuccess(res))
  .catch(error => onError('Error al obtener el usuario: ', error));
}