import React, {useState} from "react";
import TradingViewWidget, {Themes} from 'react-tradingview-widget';
import Sidebar from "../../components/SideBar"
import Selector from '../../components/Selector';

import {coinData, intervalData, intervalValues} from '../../../constants/selectorsData';

import './styles.css'
import { useEffect } from "react";

function Charts() {
  const [fromCoin , setFromCoin] = useState('BTC');
  const [toCoin, setToCoin] = useState('USDT');
  const [symbol, setSymbol] = useState('BTCUSDT');
  const [interval, setInterval] = useState('30m');

  const handleSelectToCoin = event => {
    setToCoin(event.target.value);
  }

  const handleSelectFromCoin = event => {
    setFromCoin(event.target.value);
  }  

  const handleSelectInterval = event => {
    setInterval(event.target.value);
  }

  useEffect(() => {
    setSymbol(fromCoin + toCoin);
  }, [fromCoin, toCoin, interval]);

  return (
    <div className="charts-container"> 
      <Sidebar />
      <div className="section-zone" id="charts">
        <div className="selectors-zone">
        <Selector
            value={fromCoin}
            onChange={handleSelectFromCoin}
            data={coinData}
          />
          <Selector
            value={toCoin}
            onChange={handleSelectToCoin}
            data={coinData}
          />
          <Selector
            value={interval}
            onChange={handleSelectInterval}
            data={intervalData}
          />
        </div>
        <div className="char-zone">
          <TradingViewWidget 
            autosize
            symbol={symbol}
            interval={intervalValues[interval]}
            theme={Themes.DARK}
            locale="es"
          />
        </div>
      </div>
    </div>
  )
}

export default Charts;