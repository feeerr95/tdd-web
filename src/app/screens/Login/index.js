import React, {useState} from 'react';
import {Link, useHistory} from "react-router-dom";

import {isValidEmail, isValidPassword} from '../../../utils/validations';
import Input from '../../components/Input';
import * as userService from '../../../services/userService'
import GoogleSignIn from "../../components/GoogleSignInButton";
import {ROUTES} from '../../../constants/routes';
import * as LocalStorageService from '../../../services/LocalStorageService'

import './styles.css';

function Login() {

  const history = useHistory();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleUserChange = event => setEmail(event?.currentTarget?.value);
  const handlePassChange = event => setPassword(event?.currentTarget?.value);

  const handleSubmit = event => {
      if (isValidEmail(email) && isValidPassword(password)) {
        userService.signIn({
          body: {email, password},
          onSuccess: res => {
            LocalStorageService.setValue("user", res.data);
            history.push(ROUTES.HOME)
          },
          onError: error => console.log(error)
        })
      }
    event.preventDefault();
  };
  
  return (
    <div className="loginContainer">
      <form className="loginFormContainer" onSubmit={handleSubmit}>
        <span className="title"> CryptoRules </span>
        <Input
          name="email"
          label="Email"
          className="loginInput"
          placeholder="Email"
          type="email"
          inputType="text"
          onChange={handleUserChange}
          value={email}
          required
        />
        <Input
          name="password"
          placeholder="Password"
          label="Contraseña"
          className="loginInput"
          type="password"
          inputType="text"
          value={password}
          onChange={handlePassChange}
          required
        />
        <div className="buttonsContainer">
          <button id="signup" className="button">
            <Link className="button" to="/signup">
              Registrarse
            </Link>
          </button>
          <button type="submit" id="signin" className="button">
              Ingresar
          </button>
          <GoogleSignIn  className="button" id="google" />
        </div>
      </form>
      </div>
    );
  }


export default Login;
