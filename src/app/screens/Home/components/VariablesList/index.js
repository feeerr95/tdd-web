export default function Variables (props) {
  const {
    ruleList,
    userVariablesHave
  } = props;

    return (
      ruleList.map(rule => {
        const variablesDontHave = rule.requiredVariables.filter(x => !userVariablesHave.map(v => v.name).includes(x));
        return (variablesDontHave.length !== 0) &&
        (
          <div className="rule-item">
            <span className="rule-title">{rule.name}</span>
            {
              variablesDontHave.map(variable => (
                <div className="list-item">
                  <span>{variable}</span>
                </div>
              ))
            }
          </div>
        )
      })
    )
  }