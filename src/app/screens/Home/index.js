import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import * as LocalStorageService from "../../../services/LocalStorageService"
import * as RuleService from '../../../services/ruleService';
import * as UserService from "../../../services/userService"
import {ROUTES} from '../../../constants/routes';
import Sidebar from "../../components/SideBar"
import ReactLoading from 'react-loading';
import { colors } from '../../../constants/colors';
import Selector from '../../components/Selector';
import CheckItem from '../../components/CheckItem';
import VariableList from './components/VariablesList';

import './styles.css'

function Home() {

  var user = LocalStorageService.getValue("user");

  const history = useHistory();

  const [ruleList, setRuleList] = useState([]);
  const [updateRule, setUpdateRule] = useState('');
  const [isRuleSelected, setIsRuleSelected] = useState(false);
  const [variableSelectedName, setVariableSelectedName] = useState('');
  const [variableSelectedValue, setVariableSelectedValue] = useState('');
  const [updateVariableValue, setUpdateVariableValue] = useState();

  const [userVariablesHave, setUserVariablesHave] = useState([])

  const [disabledSubmitButtonNewVariable, setDisabledSubmitButtonNewVariable] = useState(true)
  const [disableUpdateVariableButton, setDisableUpdateVariableButton] = useState(true)

  const [refresh, setRefresh] = useState(false)

  const [newVariableName, setNewVariableName] = useState();
  const [newVariableValue, setNewVariableValue] = useState();

  useEffect(() => {
    user = LocalStorageService.getValue("user");
    if (!user) {
      setRefresh(!refresh)
      history.replace(ROUTES.LOGIN)
      return
    }
    else {
      RuleService.getRules({
        accessToken: user.accessToken,
        onSuccess: res => setRuleList(res?.data.data),
        onError: response => console.log("Error al obtener las reglas: ", response)
      })
      UserService.getUser({
        accessToken: user.accessToken,
        onSuccess: res => {
          user.variables = res.data.variables
          setUserVariablesHave(res.data.variables)
        },
        onError: response => console.log("Error al obtener las variables: ", response)
      })
    }
  }, [refresh])

  useEffect(() => {
    if(!newVariableName && !newVariableValue) setDisabledSubmitButtonNewVariable(true)
    else setDisabledSubmitButtonNewVariable(false)
    if(!updateVariableValue) setDisableUpdateVariableButton(true)
    else setDisableUpdateVariableButton(false)
  })

  const handleUpdateRule = event => {
    const fileReader = new FileReader();
    fileReader.readAsText(event.target.files[0], "UTF-8");
    fileReader.onload = event => {
      setIsRuleSelected(true)
      setUpdateRule(JSON.parse(event.target.result))
    }
  };

  const handleSubmitUpdateRule = () => {
    RuleService.createRule({
      accessToken: user.accessToken,
      rule: updateRule ,
      onSuccess: () => {
        setUpdateRule('')
        setIsRuleSelected(false)
        setRefresh(!refresh)
      },
      onError: e => console.log(e)
    })
  }

  const handleSelectVariable = event => {
    userVariablesHave.map(variable => {
      if(variable.name === event.target.value) {
        setVariableSelectedName(variable.name);
        setVariableSelectedValue(variable.value);
      }
    })
  }

  const handleUpdateVariableValue = event => {
    setUpdateVariableValue(event?.currentTarget?.value);
  }

  const handleSubmitUpdateVariableValue = () => {
    UserService.updateVariable({
      accessToken: user.accessToken,
      variable: {
        name: variableSelectedName,
        value: updateVariableValue
      },
      onSuccess: () => {
        setUpdateVariableValue('')
        setRefresh(!refresh)
      },
      onError: error => console.log(error)
    })
  }

  const handleNewVariableName = event => {
    setNewVariableName(event?.currentTarget?.value);
  }

  const handleNewVariableValue = event => {
    setNewVariableValue(event?.currentTarget?.value);
  }

  const handleSubmitCreateNewVariable = () => {
    UserService.createVariable({
      accessToken: user.accessToken,
      variable: {
        name: newVariableName,
        value: newVariableValue
      },
      onSuccess: () => {
        setRefresh(!refresh);
        setNewVariableName('');
        setNewVariableValue('');
      },
      onError: error => console.log(error)
    })
  }    

  return (
    false ? (
      <ReactLoading 
        className="loader"
        type={"spin"} 
        color={colors.secundary}
        height={200} 
        width={200}
      />
    ) : (
      <div className="home-container"> 
        <Sidebar />
        <div className="home-content">
          <div className="section-zone" id="rules">
            <div className="section-title-zone">
              <span>Subir reglas</span>
            </div>
            <div className="section-content-zone">
              <div className="select-file-zone">
                <span>Seleccionar regla</span>
                <input type="file" onChange={handleUpdateRule} />
              </div>
              <div className="select-file-name-zone">
                {!!updateRule?.rules?.name && (
                  <div >
                    <span>Regla seleccionada: </span>
                    <br></br>
                    <span>{updateRule?.rules?.name}</span>
                  </div>
                )}
              </div>
              <button 
                className="section-button"
                id="upload-rule"
                disabled={!isRuleSelected}
                onClick={handleSubmitUpdateRule}
              >
                Subir regla
              </button>
            </div>    
          </div>

          <div className="section-zone">
            <div className="section-title-zone">
              <span>Reglas cargadas</span>
            </div>
            <div className="section-content-zone">
              <div>
                <ul className="scrollbar">
                  {ruleList.map(rule => (
                      <li>
                        <CheckItem className="list-item" item={rule} />
                      </li>
                    ))}
                </ul>
              </div>
            </div>
          </div>

          <div className="section-zone">
            <div className="user-check-variables-zone">
              <div className="section-title-zone">
                <span>Tus variables</span>
              </div>
              <Selector 
                className="variable-change-selector"
                data={userVariablesHave.map(v => v.name)}
                onChange={handleSelectVariable}
              />
              <input 
                className="variable-change-input"
                value={updateVariableValue}
                placeholder={variableSelectedValue}
                onChange={handleUpdateVariableValue}
              />
              <button 
                onClick={handleSubmitUpdateVariableValue}
                disabled={disableUpdateVariableButton}
                className="section-button"
              >
                Cambiar valor
              </button>
            </div>

            <div className="user-check-variables-zone">
              <div className="section-title-zone">
                <span>Agregar nueva variable</span>
              </div>
              <input 
                className="variable-change-input"
                value={newVariableName}
                placeholder="Nombre de la variable"
                onChange={handleNewVariableName}
              />
              <input 
                value={newVariableValue}
                className="variable-change-input"
                placeholder="Valor de la variable"
                onChange={handleNewVariableValue}
              />
              <button 
                onClick={handleSubmitCreateNewVariable}
                disabled={disabledSubmitButtonNewVariable} 
                className="section-button" 
              >
                Agregar variable
              </button>
            </div>
          </div>

          <div className="section-zone" id="convert">
            <div className="section-title-zone">
              <span>Variables requeridas</span>
            </div>
            <div className="section-content-zone">
              <ul className="scrollbar">
                <VariableList user={user} ruleList={ruleList} userVariablesHave={userVariablesHave} />
              </ul>
            </div>
          </div>
        </div>
      </div>
    ) 
  )
}

export default Home;


