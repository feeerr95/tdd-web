/* eslint-disable default-case */
import React, { useState } from 'react';
import { useHistory, useLocation } from "react-router-dom";
import { isValidEmail, isValidPassword, isSamePassword } from '../../../utils/validations';
import Input from '../../components/Input';
import defaultPic from '../../assets/default-user-icon.jpg'
import * as LocalStorageService from "../../../services/LocalStorageService";
import './styles.css';
import { ROUTES } from '../../../constants/routes';
import * as userService from '../../../services/userService'

function Register() {
  const history = useHistory();

  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleUserChange = event => setEmail(event?.currentTarget?.value);
  const handleNameChange = event => setName(event?.currentTarget?.value);
  const handleSurnameChange = event => setSurname(event?.currentTarget?.value);
  const handleBirthDateChange = event => setBirthDate(event?.currentTarget?.value);
  const handlePassChange = event => setPassword(event?.currentTarget?.value);
  const handleConfirmPassChange = event => setConfirmPassword(event?.currentTarget?.value);

  const handleSubmit = event => {
    if (isValidEmail(email) && isValidPassword(password) && isSamePassword(password, confirmPassword)){
      const newUser = {
        name: name,
        surname: surname,
        birthDate: birthDate,
        email: email,
        password: password,
        provider: 'email',
        profilePic: defaultPic
      }
      userService.signUp({
        body: newUser,
        onSuccess: res => {
          LocalStorageService.setValue("user", res.data);
          history.push(ROUTES.HOME);
        },
        onError: error => console.log(error)
      })
    }
    event.preventDefault();
  };

  return (
    <div className="register-container">
      <form className="register-form-container" onSubmit={handleSubmit}>
        <Input
          name="name"
          label="Nombre"
          className="register-input"
          inputType="text"
          placeholder="Nombre"
          value={name}
          onChange={handleNameChange}
          required
        />
        <Input
          name="surname"
          label="Apellido"
          placeholder="Apellido"
          className="register-input"
          inputType="text"
          value={surname}
          onChange={handleSurnameChange}
          required
        />
        <Input
          name="email"
          label="Email"
          placeholder="Email"
          className="register-input"
          type="email"
          inputType="text"
          onChange={handleUserChange}
          value={email}
          required
        />
        <Input
          name="birthDate"
          data-date-split-input="true" 
          label="Fecha de nacimiento"
          className="register-input"
          type="date" 
          value={birthDate}
          onChange={handleBirthDateChange}
          required
        />
        <Input
          name="password"
          placeholder="Contraseña"
          label="Contraseña"
          className="register-input"
          type="password"
          inputType="text"
          value={password}
          onChange={handlePassChange}
          required
        />
        <Input
          name="passwordConfirm"
          placeholder="Contraseña nuevamente"
          label="Repetir contraseña"
          className="register-input"
          type="password"
          inputType="text"
          value={confirmPassword}
          onChange={handleConfirmPassChange}
          required
        />
        <div className="register-footer-containter">
          <button type="submit" className="register-button">
            <span className="registerButtonText">
              Registrarse
            </span>
          </button>  
        </div>
      </form>
    </div>
  );
}

export default Register;
