import React, {useState, useEffect} from "react";
import Sidebar from "../../components/SideBar"
import * as LocalStorageService from '../../../services/LocalStorageService';
import * as UserService from '../../../services/userService';
import * as WalletService from '../../../services/WalletService';
import { ROUTES } from '../../../constants/routes';
import { useHistory } from "react-router-dom";
import './styles.css'

function Profile() {

  var user = LocalStorageService.getValue("user");
  const history = useHistory();

  const {
    name,
    surname,
    profilePic,
    email,
    wallet
  } = user

  var walletRegistered = !(Object.values(wallet).map(value => value? true:false).includes(false))

  const [apiKey, setApiKey] = useState(wallet.apiKey);
  const [secretKey, setSecretKey] = useState(wallet.apiSecret);
  const [isRegistered, setIsRegistered] = useState(walletRegistered);
  const [balances, setBalances] = useState([])

  useEffect(() => {
    if(isRegistered){
     WalletService.getBalance({
       wallet,
       onSuccess: res => {
          setBalances(res.data.balances)
       },
       onError: error => console.log(error)
     })
    }
  })

  const handleApiKeyInput = event => {
    setApiKey(event?.currentTarget?.value);
  }

  const handleSecretKeyInput = event => {
    setSecretKey(event?.currentTarget?.value);
  }  

  const handleCreateWallet = () => {
    WalletService.createWallet({
      accessToken: user.accessToken,
      body: {apiKey, apiSecret: secretKey},
      onSuccess: () => {
        setIsRegistered(true)
        user.wallet = {apiKey, apiSecret: secretKey}
        LocalStorageService.setValue("user", user)
        LocalStorageService.setValue("walletRegistered", true)
      },
      onError: e => console.log(e) 
    })    
  }

  const handleUnlinkWallet = () => {
    WalletService.deleteWallet({
      accessToken: user.accessToken,
      onSuccess: () => {
        setIsRegistered(false)
        user.wallet = {apiKey: '', apiSecret: ''}
        LocalStorageService.setValue("user", user)
        LocalStorageService.setValue("walletRegistered", false)
      },
      onError: e => console.log(e) 
    })
  }

  const handleLogOut = () => {
    UserService.signOut({
      accessToken: user.accessToken,
      onSuccess: () => {
        LocalStorageService.removeValue("user")
        LocalStorageService.removeValue("walletRegistered")
        history.replace(ROUTES.LOGIN);
      },
      onError: e => console.log(e)
    })
  }

  return (
    <div className="profile-container"> 
      <Sidebar />
      <div className="sections-zone">
        <div className="section-zone" id="charge-keys-zone">  
          <span className="key-title"> Inserte sus credeciales</span>
          <input 
            className="input-key" 
            placeholder={isRegistered? apiKey:"Inserte su Api Key"}
            onChange={handleApiKeyInput}
            onCopy={isRegistered? false: true}
            disabled={isRegistered? true : false}
          />
          <input 
            className="input-key" 
            placeholder={isRegistered? secretKey:"Inserte su Secret Key"}
            disabled={isRegistered? true : false}
            onCopy={isRegistered? false: true}
            onChange={handleSecretKeyInput}
          />
          <div className="buttons-wallet">
            <button className="unlink-wallet-button" onClick={handleUnlinkWallet}>
              Desvincular wallet
            </button>
            <button className="register-wallet-button" onClick={handleCreateWallet}>
              Registrar wallet
            </button>
          </div>
        </div>
        <div className="bottom">
        {
          isRegistered && (
            <div className="section-zone" id="balance">
              <span> BALANCE ACTUAL </span>
              {
                balances.map(balance => (
                  <div className="balance-container">
                    <span>{balance.asset}</span>
                    <span>{balance.free}</span>
                  </div>
                ))
              }
            </div>
          )
        }
        <div className="section-zone" id="profile">
          <img 
            src={profilePic? profilePic : ''} 
            alt="none" 
            className="profile-pic"
          />
          <span> Nombre: {name}</span>
          <span> Apellido: {surname}</span>
          <span> Email: {email}</span>
          <button 
            className="unlink-wallet-button" 
            onClick={handleLogOut}
          >
            desconectarse
          </button>
        </div>
        </div>
      </div>
    </div>
  )
}

export default Profile;