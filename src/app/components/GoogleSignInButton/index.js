import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from "../../../Firebase.js";
import { getValue, setValue } from "../../../services/LocalStorageService";
import { ROUTES } from '../../../constants/routes.js';

// Configure FirebaseUI.


const successfulGoogleLogin = () => {
    //Aca en realidad vamos a tener que pegarle al back con un auth de firebase especial
    //Le vamos a tener que pasar el firebase token y guardamos en el storage lo que nos devielva el back
    return ROUTES.HOME;
}

const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
    signInSuccessUrl: successfulGoogleLogin(),
    // We will display Google and Facebook as auth providers.
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ]
};

class SignInScreen extends React.Component {
    
    render() {
        return (
            <div>
                <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()}/>
            </div>
        );
    }
}

export default SignInScreen;