import React from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import './styles.css';

function Selector (props){
  return (
    <div>
      <Select
        className={props.className? props.className:"select-box"}
        value={props.value}
        onChange={props.onChange}
        disableUnderline
      >
        {props.data.map(symbol => (
          <MenuItem value={symbol}> {symbol} </MenuItem> 
        ))}
      </Select>
    </div>
  )
}

export default Selector;