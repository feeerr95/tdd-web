import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import BarChartIcon from '@material-ui/icons/BarChart';

export const SidebarData = [
  {
    title: "Reglas y variables",
    icon: <HomeIcon />,
    link: "/home"
  },
  {
    title: "Perfil",
    icon: <PersonIcon />,
    link: "/profile"
  },
  {
    title: "Graficos",
    icon: <BarChartIcon />,
    link: "/charts"
  },
];