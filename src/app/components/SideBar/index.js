import React from 'react';
import "./styles.css";
import { SidebarData } from './data';


function Sidebar() {

  return (
    <div className="Sidebar">
      <ul className="SidebarList">
        {SidebarData.map((val, key) => {
            return (
              <li 
                key={key} 
                className="SidebarItem"
                onClick={() => {
                  window.location.pathname = val.link
                }}
                id={window.location.pathname === val.link ? "active":""}
              >
                <div id="icon">{val.icon}</div>
                <div id="section">{val.title}</div>
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}

export default Sidebar;