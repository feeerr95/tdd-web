import React, {useState} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import * as RuleService from '../../../services/ruleService';
import * as LocalStorageService from '../../../services/LocalStorageService';

import './styles.css';

var user = LocalStorageService.getValue("user");

const WhiteCheckbox = withStyles({
  root: {
    color: `white`,
    '&$checked': {
      color: 'white',
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

function CheckItem (props) {

  const {
    name: title,
    _id: id, 
    enabled
  } = props.item

  const [checked, setChecked] = useState(enabled)

  const handleCheck = () => {
    if(!checked){
      RuleService.enable({
        accessToken: user.accessToken,
        ruleId: id,
        onSuccess: () => setChecked(true),
        onError: error => console.log(error)
      })
    }
    else {
      RuleService.disable({
        accessToken: user.accessToken,
        ruleId: id,
        onSuccess: () => setChecked(false),
        onError: error => console.log(error)
      })
    }
  }

  return(
    <div className={props.className} id="check-box">
      <span>{title}</span>
      <WhiteCheckbox
        checked={checked}
        onChange={handleCheck}
      />
    </div>
  )
}

export default CheckItem;