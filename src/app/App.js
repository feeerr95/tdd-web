import React, {Suspense} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import './App.css';
import Login from './screens/Login';
import Register from './screens/Register';
import Home from "./screens/Home/index";
import Charts from "./screens/Charts/index";
import Profile from "./screens/Profile/index";
import firebase from "../Firebase.js";
import {ROUTES} from '../constants/routes';

const App = () => (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route
              exact
              path="/"
              render={() => {
                return (
                    firebase.auth().currentUser ?
                        <Redirect to={ROUTES.HOME}/> :
                        <Redirect to={ROUTES.LOGIN}/>
                )
              }}
          />
          <Route exact path={ROUTES.REGISTER} component={Register}/>
          <Route exact path={ROUTES.LOGIN} component={Login}/>
          <Route exact path={ROUTES.CHARTS} component={Charts}/>
          <Route exact path={ROUTES.PROFILE} component={Profile}/>
          <Route exact path={ROUTES.HOME} component={Home}/>
          <Route exact path={ROUTES.EDIT_PROFILE} component={Register}/>
        </Switch>
      </Suspense>
    </Router>
);

export default App;

