import firebase from  'firebase';
import { removeValue, setValue } from "./services/LocalStorageService";
import { post } from './utils/requests';


const firebaseConfig  = {
  apiKey: "AIzaSyA6SBVKSdX_yktS6NELMKxKP7NJm7CPc10",
  authDomain: "tdd-web-e08fc.firebaseapp.com",
  projectId: "tdd-web-e08fc",
  storageBucket: "tdd-web-e08fc.appspot.com",
  messagingSenderId: "832431772434",
  appId: "1:832431772434:web:6a491ea59f391bd7e659cb",
  measurementId: "G-C8CHX5MEYR"
};
firebase.initializeApp(firebaseConfig);


firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      if (user.displayName){
        const completeName = user.displayName.split(" ");
        const currentUser = {
          name: completeName[0],
          surname: completeName[2] ? completeName[1] + " " + completeName [2] : completeName[1],
          email: user.email,
          profilePic: user.photoURL,
        };
        post({ body: currentUser, apiRoute: 'api/users/signIn/google'})
          .then(res => {
            setValue("user", res.data);
          })
          .catch(err => {
            removeValue("user")
            post({ body: currentUser, apiRoute: 'api/users/signUp/google'})
              .then(res => {
                setValue("user", res.data);
              })
              .catch(err => {
                removeValue("user")
                console.log(err)
            })
          })
      }
    } else {
        removeValue("user")
        console.log("Se cerró la sesión de google");
    }
  });

export default firebase;
