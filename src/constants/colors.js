
export const colors = {
  primaryDark: "#1c2566",
  primary: "#283593",
  primaryLight: "#535da8",
  secondaryDark: "#aa2e25" ,
  secondary: "#f44336",
  secondaryLight: "#f6685e"
}
