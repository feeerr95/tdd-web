export const coinData = [
  'BTC',
  'BNB',
  'USDT',
  'ETH'
];

export const intervalData = [
  '1m',
  '3m',
  '5m',
  '15m',
  '30m',
  '1h',
  '2h',
  '3h',
  '4h',
  '1d',
]

export const intervalValues = {
  '1m': 1,
  '3m': 3,
  '5m': 5,
  '15m': 15,
  '30m': 30,
  '1h': 60,
  '2h': 120,
  '3h': 180,
  '4h': 240,
  '1d': 1440,
}