export const ROUTES = {
  LOGIN: '/signin',
  REGISTER: '/signup',
  HOME: '/home',
  PROFILE: '/profile',
  CHARTS: '/charts',
};
